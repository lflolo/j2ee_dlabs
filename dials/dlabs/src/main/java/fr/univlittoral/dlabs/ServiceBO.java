package fr.univlittoral.dlabs;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ServiceBO {
	@Autowired
	private DaoDeal dealDAO;
	
	public List<DtoDeal>catalogDeal(){
		List<DtoDeal>list = new ArrayList<DtoDeal>();
		List<DoDeal> listDeal = dealDAO.getAllDeal();
		for(DoDeal deal : listDeal) {
			DtoDeal catalog = new DtoDeal();
			list.add(catalog);
			catalog.setId(deal.getId());
			catalog.setShopLink(deal.getShopLink());
			catalog.setImg(deal.getImgUrl());
			catalog.setTitre(deal.getTitle());
			catalog.setCreator(deal.getCreator());
			catalog.setDate(deal.getDate());
			catalog.setShopName(deal.getShopName());
			catalog.setPriceNew(deal.getPriceNew());
			catalog.setPriceOld(deal.getPriceOld());
			catalog.setPromoCode(deal.getPromoCode());
			catalog.setDescription(deal.getDescription());
		}
		return list;
	}
	
	public DtoDeal getOne(int id) {
		DoDeal deal = dealDAO.findOne(id);
		DtoDeal dtoDeal = new DtoDeal();
		dtoDeal.setTitre(deal.getTitle());
		dtoDeal.setCreator(deal.getCreator());
		dtoDeal.setDate(deal.getDate());
		dtoDeal.setDescription(deal.getDescription());
		dtoDeal.setId(deal.getId());
		dtoDeal.setImg(deal.getImgUrl());
		dtoDeal.setPriceNew(deal.getPriceNew());
		dtoDeal.setPriceOld(deal.getPriceOld());
		dtoDeal.setPromoCode(deal.getPromoCode());
		dtoDeal.setShopLink(deal.getShopLink());
		dtoDeal.setShopName(deal.getShopName());
		return dtoDeal;
	}

	public int createDeal(DtoDeal dtoDeal) {
		DoDeal newDeal = new DoDeal();
		newDeal.setTitle(dtoDeal.getTitre());
		newDeal.setDescription(dtoDeal.getDescription());
		newDeal.setPriceOld(dtoDeal.getPriceOld());
		newDeal.setPriceNew(dtoDeal.getPriceNew());
		newDeal.setImgUrl(dtoDeal.getImg());
		newDeal.setShopName(dtoDeal.getShopName());
		newDeal.setPromoCode(dtoDeal.getPromoCode());
		return dealDAO.insert(newDeal);
		}
}
