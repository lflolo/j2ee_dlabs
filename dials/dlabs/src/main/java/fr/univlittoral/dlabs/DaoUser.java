package fr.univlittoral.dlabs;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class DaoUser {

	@Autowired
	EntityManager entityManager;
		
	public List<DoUser> getAllUser() {
		Query q = entityManager.createQuery("from DoUser");		
		// TODO Auto-generated method stub
		
		List<DoUser> userList =  (ArrayList) q.getResultList();
		
		
		for(DoUser user : userList) {
			System.out.println("User: [id] = " + user.getId() + "\t"+ 
		"[pseudo] = " + user.getPseudo()+ "\t" + 
		"[firstName] = " + user.getFirstName() + "\t" +
		"[LastName] = " + user.getLastName() + "\t" +
		"[PassWord] = " + user.getPassword() + "\t");
		}
		return userList;
	}
	
	public DoUser findUser(int id) {
		Query q = entityManager.createQuery("from DoUser where id= "+id);
		return (DoUser) q.getSingleResult();
	}
	
	public int insert(DoUser user) {
		entityManager.persist(user);
		System.out.println(user.getId());
		return user.getId();
	}
	

}
