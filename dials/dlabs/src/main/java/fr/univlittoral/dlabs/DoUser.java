package fr.univlittoral.dlabs;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tbl_user")
public class DoUser {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="User_id")
	private int id;
	
	@Column(name="User_pseudo")
	private String pseudo;
	
	@Column(name="User_firstName")
	private String firstName;
	
	@Column(name="User_lastName")
	private String lastName;
	
	@Column(name="User_password")
	private String password;
	
	@OneToMany(mappedBy = "creator")
	private List<DoDeal>deals;
	
	@OneToMany(mappedBy = "idUser")
	private List<DoTemperature>temperature;
	
	public int getId() { return id;}
	public String getPseudo() { return pseudo;}
	public String getFirstName() {return firstName;}
	public String getLastName() {return lastName;}
	public String getPassword() {return password;}
	public List<DoTemperature> getListTemperature() {return temperature;}
	public List<DoDeal> getListDeal() {return deals;}
	
	public void setId(int id) {this.id = id;}
	public void setPseudo(String pseudo) { this.pseudo = pseudo;}
	public void setFirstName(String fn) { this.firstName = fn;}
	public void setLastName(String ln) { this.lastName = ln;}
	public void setPassword(String pW) {this.password = pW;}
	public void setListDeal(List<DoDeal>deal) {this.deals = deal;}
	public void setListTemperature(List<DoTemperature>temp) {this.temperature = temp;}

}
 