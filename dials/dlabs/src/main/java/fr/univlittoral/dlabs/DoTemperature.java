package fr.univlittoral.dlabs;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;

@Entity
@Table(name="tbl_temperature")
public class DoTemperature {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="temperature_id")
	private int id;
	
	@Column(name="temperature_value")
	private float value;
	
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="FK_User")
	private DoUser user;
	
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="FK_Deal")
	private DoDeal deal;
	
	public int getId() { return id;}
	public float getValue() { return value;}
	public DoUser getUser() {return idUser;}
	public DoDeal getDeal() {return idDeal;}
	
	public void setId(int id) {this.id = id;}
	public void setValue(float v) { this.value = v;}
	public void setDeal(DoUser deal) {this.deal = deal;}
	public void setUser(DoDeal user) {this.user = user;}

}