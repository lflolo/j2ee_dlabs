package fr.univlittoral.dlabs;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

public class DaoTemperature {
	
	@Autowired
	EntityManager entityManager;
		
	public List<DoTemperature> getAllTemperature() {
		Query q = entityManager.createQuery("from DoTemperature");		
		// TODO Auto-generated method stub
		
		List<DoTemperature> tempList =  (ArrayList) q.getResultList();
		
		
		for(DoTemperature temp : tempList) {
			System.out.println("Temperature: [id] = " + temp.getId() + "\t"+ 
		"[value] = " + temp.getValue()+ "\t" + 
		"[Fk_User] = " + temp.getUser() + "\t" +
		"[Fk_Deal] = " + temp.getDeal() + "\t");
		}
		return tempList;
	}
	
	public DoTemperature findTemp(int id) {
		Query q = entityManager.createQuery("from DoTemperature where id= "+id);
		return (DoTemperature) q.getSingleResult();
	}
	
	public int insert(DoTemperature temp) {
		entityManager.persist(temp);
		System.out.println(temp.getId());
		return temp.getId();
	}

}
