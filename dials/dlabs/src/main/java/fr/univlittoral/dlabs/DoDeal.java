package fr.univlittoral.dlabs;


import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_deal")
public class DoDeal {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="FK_creator")
	private DoUser creator;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="description")
	private String description;
	
	@Column(name="img_url")
	private String imgUrl;
	
	@Column(name="price_new")
	private double priceNew;
	
	@Column(name="price_old")
	private double priceOld;
	
	@Column(name="promo_code")
	private String promoCode;
	
	@Column(name="shop_link")
	private String shopLink;
	
	@Column(name="shop_name")
	private String shopName;
	
	@Column(name="title")
	private String title;
	
	@OneToMany(mappedBy = "idDeal")
	private List<DoTemperature>temperature;
			
	public int getId() { return id;}
	public DoUSer getCreator() { return creator;}
	public Date getDate() { return date;}
	public String getDescription() { return description;}
	public String getImgUrl() { return imgUrl;}
	public double getPriceNew() { return priceNew;}
	public double getPriceOld() { return priceOld;}
	public String getPromoCode() { return promoCode;}
	public String getShopLink() { return shopLink;}
	public String getShopName() { return shopName;}
	public String getTitle() { return title;}
	public List<DoTemperature>allTemp() {return temperature;}
	
	public void setId(int id) { this.id = id;}
	public void setCreatorID(DoUser creator) { this.creator = creator;}
	public void setDate(Date date) { this.date = date;}
	public void setDescription(String desc) { this.description = desc;}
	public void setImgUrl(String img) { this.imgUrl = img;}
	public void setPriceNew( double pN) { this.priceNew = pN;}
	public void setPriceOld(double pO) { this.priceOld = pO;}
	public void setPromoCode(String pC) { this.promoCode = pC;}
	public void setShopLink(String sL) { this.shopLink = sL;}
	public void setShopName(String sN) { this.shopName = sN;}
	public void setTitle(String title) { this.title = title;}
	public void setTemp(List<DoTemperature>temp) {this.temperature = temp;}
}
