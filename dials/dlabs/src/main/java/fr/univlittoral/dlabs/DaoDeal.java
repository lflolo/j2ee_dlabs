package fr.univlittoral.dlabs;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class DaoDeal {
	
	@Autowired
	EntityManager entityManager;
		
	public List<DoDeal> getAllDeal() {
		Query q = entityManager.createQuery("from DoDeal");		
		// TODO Auto-generated method stub
		
		List<DoDeal> dealsList =  (ArrayList) q.getResultList();
		
		
		for(DoDeal deals : dealsList) {
			System.out.println("Deal: [id] = " + deals.getId() + "\t"+ 
		"[FK_creator] = " + deals.getCreator()+ "\t" + 
		"[date] = " + deals.getDate()+ "\t" +
		"[description] = " + deals.getDescription()+ "\t" +
		"[image] = " + deals.getImgUrl()+ "\t" +
		"[priceNew] = " + deals.getPriceNew()+ "\t" +
		"[priceOld] = " + deals.getPriceOld()+ "\t" +
		"[promoCode] = " + deals.getPromoCode()+ "\t" +
		"[shopLink] = " + deals.getShopLink()+ "\t" +
		"[shopName] = " + deals.getShopName()+ "\t" +
		"[title] = " + deals.getTitle()+ "\t" );
		}
		return dealsList;
	}
	
	public DoDeal findOne(int id) {
		Query q = entityManager.createQuery("from DoDeal where id= "+id);
		return (DoDeal) q.getSingleResult();
	}
	
	public int insert(DoDeal doTblDeal) {
		entityManager.persist(doTblDeal);
		System.out.println(doTblDeal.getId());
		return doTblDeal.getId();
	}
	
}
