package fr.univlittoral.dlabs;

import java.util.Date;

public class DtoDeal {
	private int id;
	private String titre;
	private int temperature;
	private int creator;
	private String shopName;
	private String shopLink;
	private Date date;
	private String img;
	private String description;
	private double priceOld;
	private double priceNew;
	private String promoCode;


	public String getTitre() { return titre; }
	public void setTitre(String titre) { this.titre = titre; }
	public int getTemperature() { return temperature; }
	public void setTemperature(int temperature) {this.temperature = temperature;}
	public int getCreator() {return creator;	}
	public void setCreator(int doUser) {this.creator = doUser;	}
	public String getShopName() {return shopName;}
	public void setShopName(String shopName) {this.shopName = shopName;}
	public Date getDate() {	return date;}
	public void setDate(Date date) {this.date = date;}
	public String getImg() {return img;	}
	public void setImg(String img) {this.img = img;}
	public String getShopLink() {return shopLink;}
	public void setShopLink(String shopLink) {this.shopLink = shopLink;	}
	public String getDescription() {return description;	}
	public void setDescription(String description) {this.description = description;	}
	public double getPriceOld() {return priceOld;	}
	public void setPriceOld(double priceOld) {	this.priceOld = priceOld;	}
	public double getPriceNew() {return priceNew;	}
	public void setPriceNew(double priceNew) {this.priceNew = priceNew;	}
	public String getPromoCode() {return promoCode;	}
	public void setPromoCode(String promoCode) {this.promoCode = promoCode;	}
	public int getId() {return id;	}
	public void setId(int id) {	this.id = id;	}
	
}
