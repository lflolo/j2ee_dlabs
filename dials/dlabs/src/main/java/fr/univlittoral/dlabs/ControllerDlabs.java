package fr.univlittoral.dlabs;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/Dlabs")
@Transactional
public class ControllerDlabs {
	@Autowired
	private ServiceBO serviceBo;
		
	@RequestMapping(method = RequestMethod.GET)
	public List<DtoDeal>getAll(){
		final List<DtoDeal>deals = serviceBo.catalogDeal();
		return deals;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public DtoDeal getOne(@PathVariable Integer id) {
		return serviceBo.getOne(id);
	}
	@RequestMapping(method = RequestMethod.POST)
	public int create(@RequestBody DtoDeal dtoDeal) {
		System.out.println("Vérification recup Formulaire !! ");
		System.out.println(dtoDeal.getTitre());
		return serviceBo.createDeal(dtoDeal);
		 
	}	
}
